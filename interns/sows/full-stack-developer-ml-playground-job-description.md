## Machine Learning Playground Prototype

### Scope of Work

#### Code

- [proai_playground_backend](https://gitlab.com/tangibleai/proai_playground_backend)
- [proai_playground_frontend](https://gitlab.com/tangibleai/proai_playground_frontend)
- old prototype [in interactive IDE](https://svelte.dev/repl/25c5a42977024963accdc3cec81e252e)


### Deliverables

1. Deploy the existing prototype: 2 weeks, $400

- $100: Deploy backend to Digital-Ocean using Docker
- $100: Deploy frontend to Digital-Ocean using Docker or sveltekit or as part of the backend deployment
- $200: Add gitlab-ci.yml to [proai_playground_backend](https://gitlab.com/tangibleai/proai_playground_backend) that auto-deploys frontend and backend

2. Add user authentication: 2 weeks, $400

- $100: Svelte login component
- $100: backend login functionality with unittest
- $100: Svelte signup component
- $100: backend signup functionality with unittest

3. Display table of user's submissions, 1 week, $300

- $100: remove live update of RMSE and line within linear regression
- $100: Svelte table component that works with at least 10 columns and 100 rows of float & string values
- $100: Populate svelte table with query of the logged in user's previous submissions (slope, intercept, RMSE) for that session/game

4. 

- $100: after [Submit] update displayed table with a new row containing (slope, intercept, RMSE)

### Stretch goals

- $50: Svelte password reset
- $50:  backend models.py (User or Profile model)
- 
Add User authentication component with svelte
- $100: Deploy frontend to Digital-Ocean using Docker or sveltekit or as part of the backend deployment
- $200: Add gitlab-ci.yml to [proai_playground_backend](https://gitlab.com/tangibleai/proai_playground_backend) that auto-deploys frontend and backend


We will provide SSH credentials to VMs on DigitalOcean or an account on DigitalOcean




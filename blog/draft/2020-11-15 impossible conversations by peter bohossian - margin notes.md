2020-11-15 impossible conversations margin notes

## 1. **G**oal

Ask yourself why you are you engaged in the conversation.
Decide on an acheivable goal for the conversation before it starts.

* Rapport: Just to get to know each other
* Mutual understanding: understand the other person's perspective.
* Lean how others arrived at their conclustions
* Finding truth: correcting mistaken beliefs, untruths, facts. Get more information about a situation?
* Intervening: change somone's beliefs or methods for finding beliefs
* Impressing: conversation is a performance for the othes present
* Yielding to coercion: are you being forced into the conversation?

Difficult/Impossible to convince someone of your perspective when it's contrary to the beliefs of their social circle, family, or personal value system.
Make sure you understand what someone values and their political party, cultural heritgage, religion, before attempting to convince them of something.

## 2. **P**artner

Think of the other person as your conversation **partner** rather than oponent or rival.
Study by Frank Wesley: US soldiers that defected to North Korea were all found to have come from a boot camp that taught them that North Koreans were all horrible enemies. Others that were given balanced information about North Korean society did not voluntarily defect.

## 3. **R**apport

Building and maintaining rapport is first priority. Example questions to maintain raport:

* How do you spend your day?
* My first time here. How did you find out about this place?
* Can you say more?
* How do you spend your free time?
* What are your passions?
* books, scofi, movies, activities, sports, food?
* No parallel talk, don't compare. Ask about their experience
* Don't make their politics affect your friendship, care about them as a person
* Substantive conversation only if you have time to listen to their views
* Be ready to talk about something else
* Avoid callouts, moral shaming, shoulds etc, try to appreciate her authenticity, call-out only if you need to stand to for yourself and set boundaries or end convo
* Be courteous. Please and thank you. "I appreciate that" if they contradict you.

## 4. Listen

Listen more talk less. Put away phone, don't use it even to look up supporting information.

* let them go first at simultaneous speaking collisions
* look them in the eye, turn toward then, and nod authentically when you understand
* if the conversation gets tense, talk less, listen more
* leckeudas: wait, and pause, don't fill silence with words, pauses let people process their thoughts
* don't finish their sentences unless you think you know the word they're looking for
* Don't interrupt
* don't complete sentences
* Let other person go first
* "I hear you." often
* Explicitly remark on distractions and deal with them.

## 5. Shoot the messenger (yourself)

Don't try to deliver a message. It won't be received well if it doesn't confirm convo partner's existing beliefs.

## 5. Modeling

Model desired behavior to get past *unread library* effect

* ask questions about their understanding of a complex subject, details, what agency, what budget, where, who, what laws
* when sometime is dodging a question or obfuscating, ask them to ask you the same question about your beliefs and answer then succinctly before asking them the same question again
* say "I don't know" (honesty, sincerity, humility) and commend others who do to
* be clear
* don't interrupt, be kind and charitable, use partner words charitably
* inclusion problems at Google, only talked about diversity

## 6. Words

1. Ask for definitions and examples "how is that defined"
2. Would it mean something else in another context?
3. Accept their definitions or move on
4. Beware moral connotation s
5. Ask questions

## focus discussion on one question
1. Calibrated questions: how and what questions instead of can, is, are, does and do. No closed questions Yes/no questions. Don't cross examine
2. Just so I'm clear the question here is ... And repeat it and refine it until you get it right until they are able to state the question.
3. Revisit the initial question or come up with a new one
4. Be authentic
5. Don't use leading questions, that are actually arguments
6. Acknowledge extremists. Passion paints you as a zealot.

## recognize dogmatists

* dogmatists beliefs are inconvertible

## Phrases, ideas to learn epistemology

You can get into a learning mode with:

- "I'm trying to [understand|learn] how you know {x} so I can understand what you are saying"
- "Can you tell me more about that [|topic] so I can understand."
- "How did you come to that conclusion?"
- "I'm trying to figure out how you came to that conclusion.
- "I want to learn."
- "What more can you tell me more about that [|topic] to help me learn where you're coming from?"
- "I want to understand where you're coming from."
- "I want to understand more [|about that]."
- "I want to hear more."
- "I'd love to understand how you got to that position."
- "How can I learn more about where you're coming from?"

Vulnerability phrases:
- "I don't know."


## frame of mind

### don't

These will make you sound uncivil

1. Discourteous or uncivil
2. Display anger
3. Raise voice or talk over someone
4. Intentionally disrespect
5. Ridicule and blame someone
6. Laugh at someone
7. Attack a position before understanding it
8. Display unwillingness to hear partners arguments
9. Adopt Interpretation others words in least charitable way
10. Calling someone stupid for saying they don't understand asking questions
11. Punish mistakes or requests for help info or feedback
12. Lash out at speculation
13. Attack person rather than belief only moron could believe that"
14. View people as disruptive
15. Dishonest with yourself about what you believe
16. Pretend you know something that you don't
17. Failing to say I don't know
16. Focusing on belief and conclusions, rather than why they believe that.
18. Saying they can't really know something because of color of skin, or who they are
19. Don't change mind with new compelling evidence
20. Obfuscate
19. Delivering messages
20. Failing to acknowledge vulnerability
21. Claiming that extremists on your side are being rational
19. Correcting grammar (annoying)
20. Calling out for moral transgression in way that interupts their flow of their convo, distracts
21. Interrupt
22. Finish others' sentences for them
22. Bullly someone into convo
23. Be bullied into conversation
24. Look at phone during discussion
25. Name drop
26. Be negative and complain
23. Brag
24. Refuse to disengage until there is an earned bridge

## learning mode

If primary goal for a conversation is to be civil and get through a conversation. Learning is your go to.

1. Be explicit that you want to learn: "Im trying to understand how you know x so you can understand what they are saying" "Can you tell me more about that topic so I can understand."
2. When all else fails default to learning mode
3. Get them to doubt their epistonology
4. Learn facts to help in future vonvo


## Conversation transcripts

### Good rapport building before talking about God

```yaml
-
    Good morning! How are you? Would you happen to have 5 minutes to do a chat?
    Sure, sure!
-
    Okay, thanks. Are you okay if I livestream and record it?
    Okay. And whati's it about?
-
    Excellent question...
    [laughs]
-
    I have conversations with strangers for five minutes--
    Okay ...
-
    --to see what they believe and why.
    [brightly] Okay!
-
    And it's fun.
    Okay!
-
    Okay, thank you!
    Should I take my sunglasses off?
-
    Whatever you prefer. Whatever you're more comfortable with.
    Okay. [removes sunglasses]
-
    What is your first name?
    Kari.
-
    I'm Anthony. [extends hand for a handshake]
    Nice to meet you.
-
    Hi! Nice to meet you too! How do I spell that?
    K-A-R-I
-
    Okay ... [writes down her name for his video notes] Do you hike here a lot?
    Yes.
-
    Awesome! I've come here a couple times, but it's usually with my kids, so I can't go as fast as I want to.
    Exactly! I usually come here with my kids.
-
    Yeah, yea! They slow you down!
    Yeah.
-
    Did you cruise through it pretty fast, or ...?
    Actually, you know; this is the first time I came by myself, so it was fun to just go do something harder.
-
    Okay, well, good, good!
```

In 2 minutes Anthony had built up enough report with Kari to eventually talk with her about her beliefs in God.

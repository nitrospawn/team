# margin notes Doughnut Economics by Kate Ranworth

1. Change the goal: GDP -> Doughnut
2. Big picture thinking: embedded economy instead of self-contained market
3. Nurture human nature: rational economic man -> social adaptable humans
4. Systems approach: static equilibrium -> dynamic equilibrium (complexity)
5. Design to distribute: growth will even it up -> distributive by design
6. Regenerate: growth will clean it up -> regenerative by design
7. Growth agnostic: growth addicted -> growth agnostic

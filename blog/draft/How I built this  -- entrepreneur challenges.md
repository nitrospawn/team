2021-01-24 journal winnie jose quizbot

## Winnie

"How I built this" -- entrepreneur challenges
- find a partner
- bootstrap
- loosely related stories within each chapter


## Jose

- review quizbot
- randomize walk through graph when no exit node is specified
- keep track of questions asked so you don't ask them the same time
- create an adaptive and incremental typeform, that is DRY but checks for answer consistence, and tries to measure multi-parameter personality/intelligence/skill scores, based on tags param
- give your stack overflow profile URI - valid URL +1 https +1 valid user name +1  (then scrape karma)
- What is your favorite browser?

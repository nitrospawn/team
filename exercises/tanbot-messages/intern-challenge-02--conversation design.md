# Week 2: Conversation Design

This week you'll learn a bit about conversation design and put your learning to work by building your first Rasa chatbot story (equivalent to a Qary skill)
1. [WIP: intro to conversation design](https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/chatbots/conversation-design-overview.md)
2. [your first Rasa bot](https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/chatbots/your-first-rasa-chatbot.md)

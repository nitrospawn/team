# intern-challenge-13 Logic Puzzles

This week you'll learn how to solve logic puzzles with code.
This is GOFAI, good old fashioned AI, machines that can reason about the world.

## Resources

- [Uncle Bob's original book](https://moderatemisbehaviour.github.io/clean-code-smells-and-heuristics/)
- [Beyond Basic Python](https://inventwithpython.com/beyond/) adapts the ideas to Python

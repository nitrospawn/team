# Use python selenium to test Syndee

- pip install selenium
- download geckodriver-v0.29.1-linux64.tar.gz
- tar -xvzf geckodriver*
- mv gecko* ~/bin/  # or any directory within your PATH
- import selenium
- driver = selenium.Webdriver(url)
- `driver.find_elements_by_class` `_by_xpath`, `_by_css_selector`, `_by_partial_element_text`

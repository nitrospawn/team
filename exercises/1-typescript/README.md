# typescript


## install -via npm

TypeScript is available as a package on the npm registry available as "typescript".

You will need a copy of Node.js as an environment to run the package. Then you use a dependency manager like npm, yarn or pnpm to download TypeScript into your project.

https://www.typescriptlang.org/download

```bash
sudo npm i -g typescript
```
#### check install and version

```bash
tsc -v
```
## compile your code

use `tsc` `path/to/file`
```bash
tsc example_typescript
```

use -w or --watch to run the TypeScript compiler in watch mode
```bash
tsc -w example_typescript
```

you can write your typescript, compile it with tsc then execute it with nodejs
```bash
tsc example_typescript.ts | node example_typescript.js
```

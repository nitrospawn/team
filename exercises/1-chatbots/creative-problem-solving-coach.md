# Teaching creative thinking

## Metacognition Hints

- https://www.innovativeteachingideas.com/blog/creative-problem-solving-tools-and-skills-for-students-and-teachers
- https://blog.edclass.com/teaching-creative-problem-solving/
- https://scholarlycommons.law.cwsl.edu/cgi/viewcontent.cgi?article=1141&context=fs
- https://blog.adobe.com/en/publish/2018/09/05/5-educators-on-teaching-creative-problem-solving-in-the-classroom.html#gs.2phweu
- https://docs.google.com/presentation/d/1XFLtypm491E6UqZx0nEqR6MtUIHpygb_Pd_Yg1sykFg/edit#slide=id.p

## Coding Challenge Problem Solving

- https://www.artvoice.com/2020/10/21/how-to-improve-problem-solving-skills-in-programming/
- https://www.calltutors.com/blog/logic-in-programming/

## Business Model Ideas/Competitors

- https://www.calltutors.com
- 

## Example Problems

- [international math olympics problems](http://www.imo-official.org/problems.aspx)
- [code reading quiz questions](https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/python/code-reading-quiz.yaml)
- [skills checklist](https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/python/python-skills-checklist.md)

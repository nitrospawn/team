.
├── Mentorship
│   └── Practical Data Science Project.ipynb
├── README.md
├── Table-of-Contents--Springboard-Curriculum.md
├── Table-of-Contents--Springboard-Curriculum-Miniprojects.md
├── Unit 11
│   ├── AppleStore.csv
│   ├── Frequentist Inference Case Study - Part A (3).ipynb
│   ├── Frequentist Inference Case Study - Part B (2).ipynb
│   ├── googleplaystore.csv
│   ├── insurance2.csv
│   ├── Springboard Apps project - Tier 3 - Complete.ipynb
│   ├── Springboard Regression Case Study - the Red Wine Dataset - Tier 3.ipynb
│   └── wineQualityReds.csv
├── Unit 14
│   ├── __about__.py
│   ├── dataform.jpg
│   ├── Gradient Boosting Case Study.ipynb
│   ├── Logistic Regression Advanced Case Study.ipynb
│   ├── onelinesplit.png
│   ├── Pipfile
│   ├── Pipfile.lock
│   ├── RandomForest_casestudy_covid19.ipynb
│   ├── RRDinerCoffeeData.csv
│   ├── SouthKoreacoronavirusdataset-20200630T044816Z-001.zip
│   ├── Springboard Decision Tree Specialty Coffee Case Study - Tier 3.ipynb
│   └── titanic.csv
├── Unit 15
│   ├── Clustering Case Study - Customer Segmentation with K-Means - Tier 3.ipynb
│   ├── Cosine_Similarity_Case_Study.ipynb
│   ├── distance_dataset (1).csv
│   ├── distance_dataset.csv
│   ├── Euclidean_and_Manhattan_Distances_Case_Study.ipynb
│   └── WineKMC.xlsx
├── Unit 18
│   ├── Bayesian_optimization_case_study.ipynb
│   ├── diabetes.csv
│   ├── flight_delays_test.csv.zip
│   ├── flight_delays_train.csv.zip
│   └── GridSearchKNN_Case_Study.ipynb
├── Unit 20
│   ├── Data Story.ipynb
│   └── Data Story.pdf
├── Unit 21
│   ├── Cowboy Cigarettes Case Study - Tier 3.ipynb
│   └── CowboyCigsData.csv
├── Unit 4
│   ├── Challenge - Tier 3.ipynb
│   └── Solved.ipynb
├── Unit 6
│   ├── Data Wrangling .ipynb
│   ├── Documentation.pdf
│   ├── Exploratory Data Analysis.ipynb
│   ├── Modelling.ipynb
│   ├── Preprocessing Data.ipynb
│   ├── Skiable Presentation.pdf
│   ├── ski_data_cleaned.csv
│   ├── ski_data_step3_features.csv
│   ├── ski_resort_pricing_model.pkl
│   ├── state_summary.csv
│   └── Winninghoff PSW - Guided Capstone.pdf
├── Unit 7
│   ├── API Mini-Project.ipynb
│   └── Pandas-Profiling.ipynb
└── Unit 8
    └── Case Study: Country Club.ipynb

11 directories, 55 files
